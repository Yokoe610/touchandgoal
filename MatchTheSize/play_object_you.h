#pragma once
#include "Engine/GameObject.h"

//ロゴ画像を管理するクラス
class play_object_you : public GameObject
{
	int hPict_;		//画像番号

public:
	//コンストラクタ
	play_object_you(GameObject* parent);

	//デストラクタ
	~play_object_you();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(GameObject* pTarget) override;
};