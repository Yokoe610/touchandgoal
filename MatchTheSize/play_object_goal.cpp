#include "Engine/Image.h"
#include "play_object_goal.h"
#include "Engine/SceneManager.h"
#include "Engine/Input.h"
#include "Engine/SphereCollider.h"


//コンストラクタ
play_object_goal::play_object_goal(GameObject* parent)
	:GameObject(parent, "play_object_goal"), hPict_(-1)
{
}

//デストラクタ
play_object_goal::~play_object_goal()
{
}

//初期化
void play_object_goal::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Charactor\\GoalOb.png");
	//画面サイズを確かめる為の色違いテスト画像を表示
	//hPict_ = Image::Load("Image\\test_play_blue.png");
	assert(hPict_ >= 0);

	//初期位置(試験的)
	transform_.position_ = XMVectorSet(0.8f, 0.6f, 0, 0);

	//初期サイズ
	transform_.scale_ = XMVectorSet(0.5f, 0.5f, 0.0f, 0.0f);

	//衝突判定　位置と範囲
	SphereCollider* collision = new SphereCollider(XMVectorSet(0, 0, 0, 0), 0.05f);
	AddCollider(collision);
}

//更新
void play_object_goal::Update()
{

}

//描画
void play_object_goal::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void play_object_goal::Release()
{
}