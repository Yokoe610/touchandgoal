#pragma once
#include "Engine/GameObject.h"

//ロゴ画像を管理するクラス
class play_object_minus : public GameObject
{
	int hPict_;		//画像番号

public:
	//コンストラクタ
	play_object_minus(GameObject* parent);

	//デストラクタ
	~play_object_minus();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};