#include "PlayScene.h"
#include "play_image.h"
#include "play_object_you.h"
#include "play_object_plus.h"
#include "play_object_minus.h"
#include "play_object_goal.h"

//コンストラクタ
PlayScene::PlayScene(GameObject* parent)
	: GameObject(parent, "PlayScene")
{
}

//初期化
void PlayScene::Initialize()
{
	Instantiate<play_image>(this);
	Instantiate<play_object_you>(this);
	Instantiate<play_object_plus>(this);
	//Instantiate<play_object_minus>(this);
	Instantiate<play_object_goal>(this);
}

//更新
void PlayScene::Update()
{
}

//描画
void PlayScene::Draw()
{
}

//開放
void PlayScene::Release()
{
}
