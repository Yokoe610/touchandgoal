#include "Engine/Image.h"
#include "logo_image.h"
#include "Engine/SceneManager.h"
#include "Engine/Input.h"

const float Bs = 0.4f;	//Before_Scale
const float As = 0.8f;	//After_Scale
const float Zs = 0.0f;
const float Ms = 0.007f;//Move_Scale
const int Time = 1600;	//Wait_Time

//コンストラクタ
logo_image::logo_image(GameObject* parent)
	:GameObject(parent, "logo_image"), hPict_(-1)
{
}

//デストラクタ
logo_image::~logo_image()
{
}

//初期化
void logo_image::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Image\\LogoPictTest.png");
	assert(hPict_ >= 0);

	//初めに表示されるサイズ
	transform_.scale_ = XMVectorSet(Bs, Bs, Bs, Zs);
}

//更新
void logo_image::Update()
{
	//拡大する
	if ((transform_.scale_.vecX <= As) && (transform_.scale_.vecY <= As))
	{
		transform_.scale_.vecX += Ms;	//X軸の拡大
		transform_.scale_.vecY += Ms;	//Y軸の拡大
	}

	/*
	//エンターが押されたら
	if (Input::IsKeyDown(DIK_RETURN))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}
	*/

	//警告文無視
	//一時停止＆シーン切り替え
	else if ((transform_.scale_.vecX = As) && (transform_.scale_.vecY = As))
	{
		//時間の停止
		Sleep(Time);

		//シーンの切り替え
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}
	
}

//描画
void logo_image::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void logo_image::Release()
{
}