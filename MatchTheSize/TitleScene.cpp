#include "TitleScene.h"
#include "title_image.h"
#include "Engine/SceneManager.h"
#include "Engine/Input.h"


//コンストラクタ
TitleScene::TitleScene(GameObject* parent)
	: GameObject(parent, "TitleScene"), hPict_(-1)
{
}

//初期化
void TitleScene::Initialize()
{
	//タイトル画像の表示
	Instantiate<title_image>(this);	
}

//更新
void TitleScene::Update()
{
	//スペースキーを押してプレイシーンに遷移する
	if (Input::IsKey(DIK_SPACE)) {

		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
	}
}

//描画
void TitleScene::Draw()
{
}

//開放
void TitleScene::Release()
{
}
