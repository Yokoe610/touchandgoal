#include "ClearScene.h"
#include "clear_image.h"
#include "Engine/SceneManager.h"
#include "Engine/Input.h"


//コンストラクタ
ClearScene::ClearScene(GameObject* parent)
	: GameObject(parent, "ClearScene")
{
}

//初期化
void ClearScene::Initialize()
{
	//タイトル画像の表示
	Instantiate<clear_image>(this);
}

//更新
void ClearScene::Update()
{
	//スペースキーを押してプレイシーンに遷移する
	if (Input::IsKey(DIK_SPACE)) {

		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_LOGO);
	}
}

//描画
void ClearScene::Draw()
{
}

//開放
void ClearScene::Release()
{
}
