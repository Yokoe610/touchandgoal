#include "Engine/Image.h"
#include "clear_image.h"
#include "Engine/SceneManager.h"
#include "Engine/Input.h"


//コンストラクタ
clear_image::clear_image(GameObject* parent)
	:GameObject(parent, "clear_image"), hPict_(-1)
{
}

//デストラクタ
clear_image::~clear_image()
{
}

//初期化
void clear_image::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Image\\clear.png");
	//画面サイズを確かめる為の色違いテスト画像を表示
	//hPict_ = Image::Load("Image\\test_clear_blue.png");
	assert(hPict_ >= 0);
}

//更新
void clear_image::Update()
{

}

//描画
void clear_image::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void clear_image::Release()
{
}