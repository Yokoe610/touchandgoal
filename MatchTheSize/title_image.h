#pragma once
#include "Engine/GameObject.h"

//ロゴ画像を管理するクラス
class title_image : public GameObject
{
	int hPict_;		//画像番号

public:
	//コンストラクタ
	title_image(GameObject* parent);

	//デストラクタ
	~title_image();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};