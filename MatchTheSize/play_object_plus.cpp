#include "Engine/Image.h"
#include "play_object_plus.h"
#include "Engine/SceneManager.h"
#include "Engine/Input.h"
#include "Engine/SphereCollider.h"
#include "Engine/BoxCollider.h"


//コンストラクタ
play_object_plus::play_object_plus(GameObject* parent)
	:GameObject(parent, "play_object_plus"), hPict_(-1)
{
}

//デストラクタ
play_object_plus::~play_object_plus()
{
}

//初期化
void play_object_plus::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Charactor\\LongEnemy.png");
	//画面サイズを確かめる為の色違いテスト画像を表示
	//hPict_ = Image::Load("Image\\test_play_blue.png");
	assert(hPict_ >= 0);

	//初期位置(試験的)
	transform_.position_ = XMVectorSet(0.3f, 0.265f, 0, 0);

	//初期サイズ
	transform_.scale_ = XMVectorSet(0.5f, 0.5f, 0.0f, 0.0f);

	//衝突判定　位置と範囲 丸
	//SphereCollider* collision = new SphereCollider(XMVectorSet(0, 0, 0, 0), 0.2f);
	//AddCollider(collision);

	//衝突判定
	//引数は、コライダーの「中心位置」と「サイズ」で、
	//位置はゲームオブジェクトの原点から見た位置、
	//サイズは「幅」「高さ」「奥行き」
	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 0, 0, 0), XMVectorSet(0.0001f, 0.55f, 0, 0));
	AddCollider(collision);


}

//更新
void play_object_plus::Update()
{
	transform_.position_.vecY -= 0.008f;
	if (transform_.position_.vecY <= -0.288f) {
		transform_.position_ = XMVectorSet(0.3f, 0.27f, 0, 0);
	}
}

//描画
void play_object_plus::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void play_object_plus::Release()
{
}