#include "Engine/Image.h"
#include "play_image.h"
#include "Engine/SceneManager.h"
#include "Engine/Input.h"


//コンストラクタ
play_image::play_image(GameObject* parent)
	:GameObject(parent, "play_image"), hPict_(-1)
{
}

//デストラクタ
play_image::~play_image()
{
}

//初期化
void play_image::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Image\\play.png");
	//画面サイズを確かめる為の色違いテスト画像を表示
	//hPict_ = Image::Load("Image\\test_play_blue.png");
	assert(hPict_ >= 0);
}

//更新
void play_image::Update()
{

}

//描画
void play_image::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void play_image::Release()
{
}