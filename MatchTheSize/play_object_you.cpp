#include "Engine/Image.h"
#include "play_object_you.h"
#include "Engine/SceneManager.h"
#include "Engine/Input.h"
#include "Engine/SphereCollider.h"


//コンストラクタ
play_object_you::play_object_you(GameObject* parent)
	:GameObject(parent, "play_object_you"), hPict_(-1)
{
}

//デストラクタ
play_object_you::~play_object_you()
{
}

//初期化
void play_object_you::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Charactor\\PlayChara.png");
	//画面サイズを確かめる為の色違いテスト画像を表示
	//hPict_ = Image::Load("Image\\test_play_blue.png");
	assert(hPict_ >= 0);

	//初期位置(試験的)
	transform_.position_ = XMVectorSet(-0.8, -0.8, 0, 0);

	//初期サイズ
	transform_.scale_ = XMVectorSet(0.3f, 0.3f, 0.0f, 0.0f);

	//衝突判定　位置と範囲
	SphereCollider* collision = new SphereCollider(XMVectorSet(0, 0, 0, 0), 0.2f);
	AddCollider(collision);
}

//更新
void play_object_you::Update()
{
	//キャラクタ移動
	//上移動
	if (Input::IsKey(DIK_W)) 
	{
		if (transform_.position_.vecY <= 0.80f)
		{
			transform_.position_.vecY += 0.02f;
		}
	}
	//下移動
	if (Input::IsKey(DIK_S))
	{
		if (transform_.position_.vecY >= -0.80f)
		{
			transform_.position_.vecY -= 0.02f;
		}
	}
	//左移動
	if (Input::IsKey(DIK_A))
	{
		if (transform_.position_.vecX >= -0.84f)
		{
			transform_.position_.vecX -= 0.02f;
		}
	}
	//右移動
	if (Input::IsKey(DIK_D))
	{
		if (transform_.position_.vecX <= 0.84f)
		{
			transform_.position_.vecX += 0.02f;
		}
	}
}

//描画
void play_object_you::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void play_object_you::Release()
{
}

// 何かに当たった
void play_object_you::OnCollision(GameObject * pTarget)
{

	//当たったときの処理
	if (pTarget->GetObjectName() == "play_object_plus")
	{
		transform_.position_ = XMVectorSet(-0.8f, -0.8f, 0, 0);


		
	}

	//当たったときの処理
	if (pTarget->GetObjectName() == "play_object_minus")
	{
		if (transform_.position_.vecY != -4.0) {

			if (transform_.position_.vecY <= -4.0) {

					transform_.position_.vecY -= 0.05f;
			}
		}
		



	}

	//当たったときの処理
	if (pTarget->GetObjectName() == "play_object_goal")
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_CLEAR);

	}

}