#include "LogoScene.h"
#include "Engine/SceneManager.h"
#include "logo_image.h"

//コンストラクタ
LogoScene::LogoScene(GameObject* parent)
	: GameObject(parent, "LogoScene"),hPict_(-1)
{
}

//初期化
void LogoScene::Initialize()
{
	//拡大しながら表示、数秒後にシーン遷移
	Instantiate<logo_image>(this);	//ロゴ画像の表示
}

//更新
void LogoScene::Update()
{
}

//描画
void LogoScene::Draw()
{
}

//開放
void LogoScene::Release()
{
}
