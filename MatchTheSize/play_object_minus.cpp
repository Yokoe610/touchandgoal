#include "Engine/Image.h"
#include "play_object_minus.h"
#include "Engine/SceneManager.h"
#include "Engine/Input.h"
#include "Engine/SphereCollider.h"


//コンストラクタ
play_object_minus::play_object_minus(GameObject* parent)
	:GameObject(parent, "play_object_minus"), hPict_(-1)
{
}

//デストラクタ
play_object_minus::~play_object_minus()
{
}

//初期化
void play_object_minus::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Charactor\\SlowEnemy.png");
	//画面サイズを確かめる為の色違いテスト画像を表示
	//hPict_ = Image::Load("Image\\test_play_blue.png");
	assert(hPict_ >= 0);

	//初期位置(試験的)
	transform_.position_ = XMVectorSet(0.5f, 0, 0, 0);

	//初期サイズ
	transform_.scale_ = XMVectorSet(0.5f, 0.5f, 0.0f, 0.0f);

	//衝突判定　位置と範囲
	SphereCollider* collision = new SphereCollider(XMVectorSet(0, 0, 0, 0), 0.05f);
	AddCollider(collision);
}

//更新
void play_object_minus::Update()
{

}

//描画
void play_object_minus::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void play_object_minus::Release()
{
}