#include "Engine/Image.h"
#include "title_image.h"
#include "Engine/SceneManager.h"
#include "Engine/Input.h"


//コンストラクタ
title_image::title_image(GameObject* parent)
	:GameObject(parent, "title_image"), hPict_(-1)
{
}

//デストラクタ
title_image::~title_image()
{
}

//初期化
void title_image::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Image\\title.png");
	//画面サイズを確かめる為の色違いテスト画像を表示
	//hPict_ = Image::Load("Image\\test_title_blue.png");
	assert(hPict_ >= 0);
}

//更新
void title_image::Update()
{
	
}

//描画
void title_image::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void title_image::Release()
{
}